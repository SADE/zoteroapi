# Zotero API Module for eXistdb

## What it does
This module synchronises a Zotero group collection or personal collection and provides the data for Zotero items via JSON. 
In a nutshell it stores a local copy of a collection inside eXistdb. The Zitero data can be a private or public collection. It is designed to run periodically and after the first run it will only download new or changed items.
## Why use this modul instead of querying the Zotero API directly?
This modules stores a local copy of a collection. Thus it is faster than asking the API and it is safer in case of API changes. Also the synchronisation can be stopped and the data is still accessible, even if the project has ended and the collection has been deleted.  
  
Also any additional processing can be done easily with the data and the data can be integrated in a local search.

## Getting a Zotereo API Key
To access the Zotero API, login to the zotero.org website. Click on your username, select "Settings" and click on Feeds/API on the settings page. Now click on "Create new private key". The settings depends on your requirements. If you want to synchronise your personal library, mark the box "Allow third party to access your library" under personal library.  
If you want to synchronise a group, then you should uncheck all personal items and select per group permission, select the group you want and give read permissions.  
Click save and save the key. You will need it later.  
You will also need your user id or group id, depending on what you want to synchronise. To get your user id (this is not your user name), go to the settings packe under Feeds/API and look for "Your userID for use in API calls: ..."  
To get the group id, Click on your username, select my profile and look for your group on the right side. Click on your group. Now take a look at your browser url. It will look like https://www.zotero.org/groups/123456/your-group-name. The group id in this example is 123456 right before your group name.

## How to install and configure
You can build this module with ant by simply executing ant on the directory or you can download a pre build package from the build system.  
Now simply install the XAR with the existdb package manager (web GUI) or moving the XAR into the autodeploy directory of the exist db installation directory.
  
### Configuration
Please open exide, navigate to /db/apps/zoteroapi and open config.xml and change these values:

```xml
<config xmlns="http://sade.textgrid.de/ns/zoteroapi/configfile">
    <param key="zotero.url">https://api.zotero.org</param>
    <param key="zotero.APIKey">YOUR-ZOTERO-API-KEY</param>
    <param key="zotero.userOrGroupID">YOUR-USER-OR-GROUPID-TO-SYNC</param>
    <param key="zotero.syncType">groups</param>
    <param key="zotero.datadirRoot">/db/apps/zoteroapi</param>
    <param key="zotero.datacollection">zoterodata</param>
    <param key="zotero.existuser">admin</param>
    <param key="zotero.existpassword"></param>    
</config>
```
* zotereo.url: URL to th zotero API. Leave it as it is.
* zotereo.APIKey: insert your API key here. See "Getting a Zotereo API Key".
* zotero.userOrGroupID: numerical value of your user id or group id to sync. See "Getting a Zotereo API Key" on how to find the ID.
* zotero.syncType: can be "users" if you want so synchronise a personal library or "groups" if you want to synchronise a group.
* zotero.datadirRoot: Where to store the data inside the database. Default is under this application. Can be changed if needed.
* zotero.datacollection: folder name of the collection for storing the data. Default is zoterodata (/db/apps/zoteroapi/zoterodata).
* zotero.existuser: user that has permission to create a new collection.
* zotero.existpassword: password for the user that has permissions to create a new collection

You can avoid storing passwords in the config.xml file if you manually create the data collection that has write permissions for the guest account. E.g. create a new collection inside exide and execute
`let $chmod1 := sm:chmod(xs:anyURI("/db/apps/zoteroapi/zoterodata"), "rwxrwxrwx")`
`let $chmod2 := sm:chmod(xs:anyURI("/db/apps/zoteroapi/zoterodata/json"), "rwxrwxrwx")`
You have to create a json collection inside the main collection.

## Setting up the cron job
Sadly existdb does not store cron jobs permanently via the xquery API (like addscheduler.xq and removescheduler.xq) do.  
You need to add the cron job to the conf.xml file of your database installation. You need to make shure that the scheduler module is enabled, look under extension if
`<module uri="http://exist-db.org/xquery/scheduler" class="org.exist.xquery.modules.scheduler.SchedulerModule"/>`
is on.  
To add a cron job, go to the `<scheduler>` section and add a new entry like:  
`        <job xquery="/db/apps/zoteroapi/modules/zoteroscheduler.xq"  cron-trigger="0 0 1 * * ?"/>
`
Which will start the synchronisation every day at 1am. See [the exist documentation](https://exist-db.org/exist/apps/doc/scheduler) for setting up the cron values.

## How to use it
The API is very basic. Either work with the data directory directly or use the REST API to get a zotero item via JSON like
`http://yourexisturl.example/exist/restxq/zotero/item/<ID-OF-A-ZOTERE-ITEM`
Example  
`http://localhost:8080/exist/restxq/zotero/item/22CXKUUA`
You can see the ID of a zotero item if you open your zotero.org library on the website, open the item and look at the URL of your browser.  
In our use case the editors insert biblographic references into TEI XML Data via zotero:ID-OF-A-ZOTERE-ITEM.  
It is also possible to retrieve multiple items at once. use
`http://yourexisturl.example/exist/restxq/zotero/items/<ID-OF-A-ZOTERE-ITEM1,<ID-OF-A-ZOTERE-ITEM1`  
and seperate the IDs via a comma.

### REST API not working
Sometimes existdb fails on registering RESTXQ modules. Simply execute the registerrest.xq from zoteroapi in exide. 

### Run Zotero API locally in SADE projects
To run the Zotero API locally in a SADE project you have to start the exist database and open `db/app/zoteroapi/config.xml` in eXide. Paste in the values for `zotero.userOrGroupID` and `zotero.APIKey`.
After that, open the file `db/app/zoteroapi/modules/zoteroscheduler.xq` and launch the script (button "Eval" in the menubar of eXide). It takes a while to create the references, the script confirms it's run with the message `<true/>` in the terminal of eXide (the references are stored in `db/app/zoteroapi/zoterodata/json`).

## License ##
Apache 2.0.
