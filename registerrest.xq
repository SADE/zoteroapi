xquery version "3.1";

(:~
 :  ____    _                 _   ___ ___    __                   _    _      _ _    
 : |_  /___| |_ ___ _ _ ___  /_\ | _ \_ _|  / _|___ _ _   _____ _(_)__| |_ __| | |__ 
 :  / // _ \  _/ -_) '_/ _ \/ _ \|  _/| |  |  _/ _ \ '_| / -_) \ / (_-<  _/ _` | '_ \
 : /___\___/\__\___|_| \___/_/ \_\_| |___| |_| \___/_|   \___/_\_\_/__/\__\__,_|_.__/
 :                                                                                  
 : Helper function for registering the restxq interface. Sometimes the auto deployment does not work.
 : Simply execute this via exide.
 :
 : @author Johannes Biermann
 : @version 1.0
 :)



import module namespace xdb="http://exist-db.org/xquery/xmldb";

import module namespace xrest="http://exquery.org/ns/restxq/exist" at "java:org.exist.extensions.exquery.restxq.impl.xquery.exist.ExistRestXqModule";


(: Register restxq modules. Should be done via post install, but sometimes it does not work :)
exrest:deregister-module(xs:anyURI('/db/apps/zoteroapi/modules/rest.xql')),
xrest:register-module(xs:anyURI("/db/apps/zoteroapi/modules/rest.xql")),
rest:resource-functions()