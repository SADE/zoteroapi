xquery version "3.1";

(:~
 :  ____    _                 _   ___ ___    __                   _    _      _ _    
 : |_  /___| |_ ___ _ _ ___  /_\ | _ \_ _|  / _|___ _ _   _____ _(_)__| |_ __| | |__ 
 :  / // _ \  _/ -_) '_/ _ \/ _ \|  _/| |  |  _/ _ \ '_| / -_) \ / (_-<  _/ _` | '_ \
 : /___\___/\__\___|_| \___/_/ \_\_| |___| |_| \___/_|   \___/_\_\_/__/\__\__,_|_.__/
 :                                                                                  
 : Main module of the zotero api which does all the necessary work.
 :
 : @author Johannes Biermann
 : @version 1.0
 :)


module namespace zotero="http://sade.textgrid.de/ns/zoteroapi/zotero";

declare namespace templates="http://exist-db.org/xquery/templates";

declare namespace repo="http://exist-db.org/xquery/repo";
declare namespace expath="http://expath.org/ns/pkg";

declare namespace cf="http://sade.textgrid.de/ns/zoteroapi/configfile";
import module namespace config="http://sade.textgrid.de/ns/zoteroapi/config" at "config.xqm";

declare namespace http="http://expath.org/ns/http-client";


declare variable $zotero:httprequestget := <http:request method='get'>
                                        <http:header name='Zotero-API-Key' value='{config:get("zotero.APIKey")}'/>                                                     </http:request>;
declare variable $zotero:collection := config:get("zotero.datadirRoot") || "/" || config:get("zotero.datacollection");

declare variable $zotero:baseURL := config:get("zotero.url") || "/" 
            || config:get("zotero.syncType") || "/" || config:get("zotero.userOrGroupID");

declare variable $zotero:style := config:get("zotero.style");
declare variable $zotero:language := config:get("zotero.language");

(:
 This function fetches a single zotero item which is identified by an id.
 An item represents any item in a zotero library. You can manually get the 
 id if you look at the URL at the zotero web page. Via the API
 the ids are returned, so normally you don't need to take care about this.
 
 Zotero can generate different formats. This function retrieves common formats
 and they are returned via JSON. Each entry is saved as JSON file in the database
 
 @param $itemId Zotero item ID to fetch and store
 @return true or false 
 
 :)
declare function zotero:fetchAndSaveItem($itemId as xs:string) as xs:boolean {
    let $col := zotero:prepareCol()
    let $url := $zotero:baseURL
                  || "/items/" || $itemId|| "?format=json&amp;include=data,bib,citation,tei&amp;style=" || $zotero:style || "&amp;locale=" || $zotero:language 

    let $result := http:send-request($zotero:httprequestget, $url)
    
    let $json := if ($result[1]/@status eq "200")
    then
        parse-json(util:base64-decode($result[2]))
    else (false())

    let $store := if ($result[1]/@status eq "200") then
        xmldb:store(
            $zotero:collection || "/json",
            $itemId || ".json",
                  serialize($json,map { 'method':'json'})
        )
    else
        false()
    
    return true()
};

(:
 This function fetches all Zotero items from a configured group ID
 It calls the Zotero API to get all IDs from the group,
 then fetches each item and stores the item individually in a json file.
 The Zotero API contains a version number in the HTTP header.
 This version number will be used to save a state file. The next time
 this function is executed it will only fetch new/changed items so
 this function can be executed with the existdb scheduler
:)
declare function zotero:fetchAllItemIDsAndSave() as xs:boolean {
    let $col := zotero:prepareCol()
    let $lastSyncVer := zotero:getStatus()/zoteroapi/sync[1]/@version
    (: 
    get a list of ids of zotero entries for a certain version
    on initial sync this is a list of all ids, after second sync
    only new/changed items since the last sync 
    excluding itemType note because fetching details ahout this item
    with the format tei raises an error in the zotero api
    ideally attachements would be skipped, too, but the boolean operator
    -note || -attachement according to the zotero api documentation is not
    working. The attachements itself will not be downloaded so this is not an 
    issue. 
    :)
    let $url := $zotero:baseURL
                 || "/items?itemType="||encode-for-uri('-note')||"&amp;format=versions&amp;since="||encode-for-uri($lastSyncVer)


    let $result := http:send-request($zotero:httprequestget, $url)

    let $json := if ($result[1]/@status eq "200")
    then
        parse-json(util:base64-decode($result[2]))
    else ()
    

    let $results := $result[1]//*[@name="total-results"]/@value
    let $version := $result[1]//*[@name="last-modified-version"]/@value

    let $items := if (($result[1]/@status eq "200") and $results > 0) then
        fn:for-each(
            map:keys($json),
            function($k) { zotero:fetchAndSaveItem($k) }
        )
    else
        ()
    
    (: Only update status if HTTP Request was successfull, 
    if not it will try again on the next run
    :)
    let $statusUpdate := if ($result[1]/@status eq "200") then 
        zotero:updateStatus($version)
    else
        ()


    return true()
};

(: 
 This function ask the zotero api for giving a list of ids which
 have been deleted since the last sync process. This will delete
 the resource from the local data directory, too.
:)
declare function zotero:removeDeletedItems($version as xs:string) as xs:boolean {
    (: if the version is 0=all valid keys, then there are no deleted items:)
    if ($version = 0)
        then 
         true()
    else
        let $url := $baseURL
                 || "/deleted?since="||encode-for-uri($version)

        let $result := http:send-request($zotero:httprequestget, $url)

        let $json := if ($result[1]/@status eq "200")
        then
            parse-json(util:base64-decode($result[2]))
        else
           null
        
        let $j := if ($json) then
            map:for-each($json, function($key, $value) 
            { 
                xmldb:remove($zotero:collection, $key || ".xml")
            }) 
        else 
            ()
            
        
    return true()
};

(: 
This function is responsible for returning a status file for the synchronisation
The status file is named status.xml under the specified data directory
If the file does not exist (initial sync) it will create a new file
:)
declare function zotero:getStatus() as document-node() {
    let $cAvail := zotero:prepareCol()
    let $status := "status.xml"
    let $statusUri := $zotero:collection || "/" || $status
    
    let $available := if (not(doc-available($statusUri))) then
            (xmldb:store($zotero:collection, $status, <zoteroapi/>),
            update insert <sync version="0" timestamp="{current-dateTime()}"/> 
            into doc($statusUri)/zoteroapi)
        else ()
    
    let $statusDoc := doc($statusUri)
    
    return $statusDoc
};

(: sets the last synchronized version into the status file
  and sets the last synchronisation date
  @param $version the zotero version number of the library
:)
declare function zotero:updateStatus($version as xs:string) {
    
    let $status := "status.xml"
    let $statusUri := $zotero:collection || "/" || $status
    
    return 
        update replace doc($statusUri)//zoteroapi/sync[1] with <sync version="{$version}" timestamp="{current-dateTime()}"/> 

};



(: 
creates a collection for storing the JSON data from zotero.
Path can be changed by modifing config.xml 
:)
declare function zotero:prepareCol() as xs:boolean {
let $col := config:get("zotero.datadirRoot") || "/" || config:get("zotero.datacollection")
return
    if(xmldb:collection-available(xs:anyURI($zotero:collection) )) then true()
    else
        let $login := xmldb:login($config:app-root, config:get("zotero.existuser"), config:get("zotero.existpassword") )
        let $do := (xmldb:create-collection(config:get("zotero.datadirRoot"), config:get("zotero.datacollection")),
                    xmldb:create-collection($zotero:collection, "json"))
        let $chmod1 := sm:chmod(xs:anyURI($zotero:collection), "rwxrwxrwx")
        let $path := $zotero:collection || "/json"
        let $chmod2 := sm:chmod(xs:anyURI($path), "rwxrwxrwx")
        return true()
        
};
