xquery version "3.1";

(:~
 :  ____    _                 _   ___ ___    __                   _    _      _ _    
 : |_  /___| |_ ___ _ _ ___  /_\ | _ \_ _|  / _|___ _ _   _____ _(_)__| |_ __| | |__ 
 :  / // _ \  _/ -_) '_/ _ \/ _ \|  _/| |  |  _/ _ \ '_| / -_) \ / (_-<  _/ _` | '_ \
 : /___\___/\__\___|_| \___/_/ \_\_| |___| |_| \___/_|   \___/_\_\_/__/\__\__,_|_.__/
 :                       
 : This XQ runs fetchAllItemIDsAndSave. It can be executed manually to download new
 : items or better use the scheduler from existdb to run this XQ periodically.
 :  After the next run it will only download new items.
 : See README.md on how to setup the scheduler.
 : @author Johannes Biermann
 : @version 1.0
 :)

import module namespace zotero="http://sade.textgrid.de/ns/zoteroapi/zotero" at "zotero.xqm";
import module namespace config="http://sade.textgrid.de/ns/zoteroapi/config" at "config.xqm";


let $datetime := current-dateTime()
let $message := concat('Zotero Synchronisation Cron Job, current date-time :  ', $datetime)
let $log := util:log-system-out($message)

let $dir := config:get("zotereo.datadirRoot") 
        || "/" || config:get("zotereo.datacollection")

let $test := if (xmldb:touch($dir, "json")) then
    ()
else 
    xmldb:login($dir, config:get("zotereo.existuser"), config:get("zotereo.existpassword"))

let $result := zotero:fetchAllItemIDsAndSave()

let $c := count(collection($dir || '/json/'))

let $message := concat($test, 'Zotero Synchronisation Done, current date-time :  ', 
        $datetime, ' last zotero version: ', 
        doc($dir || "/status.xml")/zoteroapi/sync[1]/@version,
        ' documents in collection: ', $c
    )
    
    
let $log := util:log-system-out($message)

return <true/>